window.onload = () => {
  console.log('Main.js triggered');
//------------------
//  Variables
//------------------

  // Tooltip container for intro and other hints
  const tooltipContainer = document.getElementById('js--tooltipContainer');
  const tooltipText = document.getElementById('js--tooltipText');
  const tooltipContinueButton = document.getElementById('js--tooltipContinue');

  const helpButton = document.getElementsByClassName('js--helpButton');

  const basicButtons = document.getElementsByClassName('js--basicButton');

  const coolingTimer = document.getElementById('js--coolingTimer');
  const coolingInteractionPane = document.getElementById('js--coolingInteractionPane');
  const kraanAnimatie = document.getElementById('js--kraanScene');

  const cleaningText = document.getElementById('js--cleaningText');
  const cleaningInteractionPane = document.getElementById('js--cleaningInteractionPane');
  const cleaningProgressBar = document.getElementById('js--cleaningProgressBar');
  const bandageButton2 = document.getElementById('js--bandageButton2');
  const bandageStart2 = document.getElementById('js--bandageStart2');
  const cameraBandage2 = document.getElementById('js--cameraBandage2');
  // const cameraEntity = document.querySelector('#js--camera');
  const cameraEntity = document.getElementById('js--camera');
  const armEntity = document.getElementsByClassName('js--arm');
  const bandagingEntity= document.querySelector('#js--bandagingEntity');

  const arm = document.getElementById('js--arm');
  const armVerbrand = document.getElementById('js--armVerbrand');
  const armBebloed = document.getElementById('js--armBebloed');
  const armVerband = document.getElementById('js--armVerband');

  const verbanddoos = document.getElementById('js--verbanddoos');
  const verbanddoosDeur = document.getElementById('js--verbanddoosDeur');
  const phone = document.getElementById('js--phone');
  const phone2 = document.getElementById('js--phone2');
  const verband = document.getElementById('js--verband');
  const verband2 = document.getElementById('js--sceneBandage2');
  const kompres = document.getElementById('js--kompres');
  const cameraBandages = document.getElementsByClassName('js--cameraBandages');
  const sceneBandages = document.getElementsByClassName('js--sceneBandage');
  const bandageStart = document.getElementById('js--bandageStart');
  const middel = document.getElementById('js--desinfectie');
  const verbanddoosDeur2 = document.getElementById('js--verbanddoosDeur2');

  const bandageButton = document.getElementById('js--bandageButton');

  const chalkboard = document.getElementById('js--chalkboard');
  const level1 = document.getElementById('js--level1');
  const level2 = document.getElementById('js--level2');
  const textLevel1 = document.getElementById("js--textLevel1")
  const textLevel2 = document.getElementById("js--textLevel2")
  const sceneLevel1 = document.getElementById("js--kitchenScene");
  const sceneLevel2 = document.getElementById("js--kitchenScene2");

  const ontsmetStart = document.getElementById("js--ontsmetStart");
  const ontsmetButton = document.getElementById("js--ontsmetButton");
  const ontsmetAnimation = document.getElementById("js--ontsmettingScene");

  const certificate = document.getElementById('js--certificate');
  const restartButton = document.getElementById('js--restartButton');
  const restart = document.getElementById('js--restart');

  //Holds the value of the current step
  //starts at 0 but for testing purposes of cooling interaction set to 2
  let step = 0;

  //boolean value which checks if you moved to the tap, standard = false
  let movedToTap = false;

  //boolean value to verify if you're cooling
  let currentlyCooling = false;

  //boolean value to verify if you bandaged your arm
  let bandagedArm = false;

  //boolean value to verify if you picked up the bandages
  let clickedVerband = false;

  //chalkboard tonen nadat het level is doorlopen
  let nextLevel = false;

  //-----------------
  //TOOLTIP CONTAINER
  //-----------------


  for(let i = 0; i != helpButton.length; i++){
    helpButton[i].addEventListener('click', () => {
      tooltipTextHandler();
    })// E L
  }

  tooltipContinue = (stap) => {
    tooltipContainer.setAttribute('visible','false');
    //toont het menu wanneer het level is doorlopen
    switch(stap){
      case 'done':
        // chalkboard.setAttribute('visible', true);
        chalkboard.setAttribute("animation", "property: position; easing: linear; dur: 10; to: 0 0 0" );
        setTimeout(function() {
          sceneLevel1.setAttribute("animation", "property: position; easing: linear; dur: 10; to: 0 10 0");
          sceneLevel1.setAttribute("visible","false");
          chalkboard.setAttribute("visible","true");
          sceneLevel2.setAttribute("animation", "property: position; easing: linear; dur: 1000; to: 0 10 0" );
          sceneLevel2.setAttribute("visible","false");
        },20);
        step++;
        armVerband.setAttribute('visible','false');
        arm.setAttribute('visible','true');
        break;
      case 'finished':
        certificate.setAttribute('visible','true');
        break;
    }
    }
  //tooltipContinue
  //handler to show specific text in tooltip container
  tooltipTextHandler = () => {
    //checks which step you're at, if you need a tooltip on this step, display tooltip
    switch(step) {
      case 1:
        editAndShowTooltip("Let eerst op gevaar, zorg dat er \nniet meerdere mensen gewond \nkunnen raken.");
        responsiveVoice.speak("Let eerst op gevaar, zorg dat er niet meerdere mensen gewond kunnen raken.","Dutch Male");
        break;
      case 2:
        editAndShowTooltip("Zoek een manier om je \nbrandwond te koelen.");
        responsiveVoice.speak("Zoek een manier om je brandwond te koelen.","Dutch Male");
        break;
      case 3:
        editAndShowTooltip("Omdat je brandblaren hebt is \nhet verstandig de wond af \nte dekken. Zoek iets om je \nwond mee af te dekken");
        responsiveVoice.speak("Omdat je brandblaren hebt is het verstandig de wond af te dekken. Zoek iets om je wond mee af te dekken","Dutch Male");
        break;
      case 4:
        setTimeout(function() {
          editAndShowTooltip("Goed gedaan, je hebt dit\nlevel gehaald!");
          responsiveVoice.speak("Goed gedaan, je hebt dit level gehaald!","Dutch Male");
        },1000);
        step++;
        textLevel1.setAttribute("color", "grey");
        textLevel2.setAttribute("color", "white");
        level1Voldaan = true;
        console.log(step);
        break;
      case 6:
        editAndShowTooltip("Zoek een manier om\nje wond schoon te maken");
        responsiveVoice.speak("Zoek een manier om je wond schoon te maken","Dutch Male");
        break;
      case 7:
        editAndShowTooltip("Zoek een manier om\nje wond te desinfecteren");
        responsiveVoice.speak("Zoek een manier om je wond te desinfecteren","Dutch Male");
        break;
      case 8:
        editAndShowTooltip("Zoek een manier om\nje wond af te dekken");
        responsiveVoice.speak("Zoek een manier om je wond af te dekken","Dutch Male");
        break;
    }//switch
  } //tooltipTextHandler

  editAndShowTooltip = (text) => {
    tooltipText.setAttribute('value',text);
    tooltipContainer.setAttribute('visible',true);
  } // editAndShowTooltip

  //-----------------
  //EINDE TOOLTIP CONTAINER
  //-----------------





//--------------------------------------------------------------------------------------------
  //----------------
  // level 1
  //----------------

    let level1Voldaan = false;
    //start het eerste level
    level1.addEventListener('click', () => {
      if(level1Voldaan == false){
        console.log("level1");
        let att = document.createAttribute("animation");
        //att.value = "property: position; easing: linear; dur: 500; to: 0 -5 0 "
        sceneLevel1.setAttribute("animation", "property: position; easing: linear; dur: 10; to: 0 0 0" );
        setTimeout(function() {
          chalkboard.setAttribute("animation", "property: position; easing: linear; dur: 10; to: 0 10 0");
          chalkboard.setAttribute("visible","false");
          sceneLevel1.setAttribute("visible","true");
        },20);
        tooltipContainer.setAttribute("visible", true);
        responsiveVoice.speak("Je hebt je arm verbrand aan de oven. de oven heb je al uitgezet. wat is de volgende stap die je gaat maken?","Dutch Male");
        arm.setAttribute('visible',false);
        armVerbrand.setAttribute('visible',true);
        step += 1;
      }else{
        console.log("Level 1 is voldaan")
      }

    })

    //Restart Level
    let level2Voldaan = false;
    restartButton.addEventListener('click', () => {
        if(level2Voldaan == true){
          document.location.reload(true);
        }else{
          console.log("level 2 niet voldaan")
        }
    })

    //when event triggered, check if there needs to be a tooltip visible
    let checkingArm = false
    tooltipContinueButton.addEventListener("click", () => {
     //checkt of de persoon alles heeft doorlopen bij stap 5 geeft het
     //een seintje door zodat het menu weer wordt getoont.
     if(step == 1){
       if(checkingArm == false){
         emitEventOnEntity(armVerbrand,'checkArm');
         responsiveVoice.speak('Het lijkt erop dat je brandblaren hebt. dit betekend dat je een tweede graads brandwond hebt',"Dutch Male");
         tooltipContinue();

         setTimeout(function() {emitEventOnEntity(armVerbrand,'stopCheckArm'); step++},4000);
         checkingArm = true;
       }
     }else if(step == 5){
       tooltipContinue('done');
     }else if(step == 9){
       tooltipContinue('finished');
     }else{
       tooltipContinue();
     }
    })// E L


  //-----------------
  //COOLING INTERACTIE
  //-----------------

  let messageRecieved = false;
  coolingInteractionPane.addEventListener('click', () => {
    // emitEventOnEntity(cameraEntity,'moveToTap');
    coolingHandler();
    if(step == 2 && movedToTap == true && messageRecieved == false){
      coolingTimerCountdown('cooling');
      tooltipContainer.setAttribute("visible", false);
      responsiveVoice.speak("Koel je brandwond 10 minuten lang, met lauw warm water","Dutch Male");
      messageRecieved = true;
    }
  })// E L

  coolingInteractionPane.addEventListener('mouseenter', () => {
    if(step == 2 && movedToTap == true){
      coolingTimerCountdown('cooling');
    }else{console.log('coolingInteractionPane mouseenter else');}
  })// E L

  coolingInteractionPane.addEventListener('mouseleave', () => {
    currentlyCooling = false;
    if(step == 2 && movedToTap == true && currentlyCooling == true){
      coolingTimerCountdown('notCooling');
    }else{console.log('coolingInteractionPane mouseleave else');}
  })// E L

  // event emitted when camera is moved TO tap
  // when timer = 0, camera will return to start position
  cameraEntity.addEventListener('animationcomplete__go1', () => {
    for(let i = 0; i != armEntity.length; i++){
      emitEventOnEntity(armEntity[i],'positionTap');
    }
  })// E L

  cameraEntity.addEventListener('animationcomplete__go2', () => {
    for(let i = 0; i != armEntity.length; i++){
      emitEventOnEntity(armEntity[i],'positionStart');
    }
  })// E L

   //checks which step you're at and if you need to interact with the tap
   coolingHandler = (e) => {
    if(step == 2 && movedToTap == false) {
      //emit event to move to tap
      emitEventOnEntity(cameraEntity,'moveToTap');
      movedToTap = true;
      coolingTimer.setAttribute('visible','true');
      kraanAnimatie.setAttribute("visible", true);
    }else{console.log('Not at step 2 yet, or already at tap');}
  }

   // variables defined for usage in coolingTimerCountdown
   let miliseconds = 60;
   let seconds = 0;
   let minutes = 10;

   // the timer functionality that is paused and played with the function coolingTimerCountdown
   let timer = setInterval(() => {
     if(currentlyCooling == true){
       if(miliseconds <= 0){
         seconds -= 6;
         miliseconds = 60;
       } else if(seconds <= 0){
         minutes -= 1;
         seconds = 60;
       }else if(minutes <= 0){
         clearInterval(timer);
         coolingTimer.setAttribute('value','00:00:00');
         minutes = 0;
         seconds = 0;
         miliseconds = 0;
         step += 1;
         console.log(step);
         emitEventOnEntity(cameraEntity,'goBack');
         coolingTimer.setAttribute('visible','false');
         kraanAnimatie.setAttribute('visible', false);
       }
       // to localeString for double digits when counting down e.g.: 00:00:00
       let format = minutes.toLocaleString(undefined,{minimumIntegerDigits: 2}) + ':' + seconds.toLocaleString(undefined,{minimumIntegerDigits: 2}) + ':' + miliseconds.toLocaleString(undefined,{minimumIntegerDigits: 2});
       miliseconds -= 12;
       coolingTimer.setAttribute('value',format);
     }else{'not currently cooling'}
   },1); //setInterval

   // the function compatible with the timer setinterval
   coolingTimerCountdown = (status) => {
     switch(status){
       case 'cooling':
         currentlyCooling = true;
         break;
       case 'notCooling':
         currentlyCooling = false;
         clearInterval(timer);
         break;
     }
   }// coolingTimerCountdown

  //-----------------
  //EINDE COOLING INTERACTIE
  //-----------------

  //-----------------
  //VERBAND INTERACTIE
  //-----------------

  bandageButton.addEventListener('click', () => {
    if(step == 3 && bandagedArm == false && clickedVerband == true) {
      for(let i = 0; i != sceneBandages.length; i++){
        sceneBandages[i].setAttribute('visible','false');
      }//for
      bandageStart.setAttribute('visible','false');
      responsiveVoice.speak("Leg het niet verklevend wondkompres op de wond. Verbind de wond daarna met het dekverband","Dutch Male");
      for(let i = 0; i != cameraBandages.length; i++){
        cameraBandages[i].setAttribute('visible','true');
        // emitEventOnEntity(armEntity, 'startBandaging');
        for(let i = 0; i != armEntity.length; i++){
          emitEventOnEntity(armEntity[i],'startBandaging');
        }
        emitEventOnEntity(armVerbrand, 'startBandaging');
        emitEventOnEntity(cameraBandages[i], 'startBandaging');
      }//for
    }else{console.log('bandageButton else');}
  })//bandagebutton
  verbanddoos.addEventListener('click', () => {
    console.log("verbanddoos");
    openBox();
  })

  verbanddoosDeur.addEventListener('click', () => {
    console.log("verbanddoosDeur");
    openBox();
  })


  verband.addEventListener('click', () =>{
    if(step == 3 && clickedVerband == false){
      let att = document.createAttribute("animation");
      let att2 = document.createAttribute("animation");
      att.value = "property: position; easing: linear; dur: 1000; to: 0.7 1.4 -3 "
      att2.value = "property: position; easing: linear; dur: 1000; to: 0.8 1.6 -2"
      verband.setAttribute("animation", att.value);
      kompres.setAttribute("animation", att2.value);
      bandageStart.setAttribute('visible','true');
      tooltipContainer.setAttribute("visible", false);
      clickedVerband = true;
    }
  });


  bandagingEntity.addEventListener('animationcomplete__5', () => {
    for(let i = 0; i != cameraBandages.length; i ++) {
      cameraBandages[i].setAttribute('visible','false');
    }
    armVerbrand.setAttribute('visible','false');
    armVerband.setAttribute('visible','true');
    // emitEventOnEntity(armEntity, 'stopBandaging');
    for(let i = 0; i != armEntity.length; i++){
      emitEventOnEntity(armEntity[i],'stopBandaging');
    }
    step ++;
    bandagedArm = true;
    tooltipTextHandler();
  })



  //Emits events on entity for animation purposes (a-frame startEvents)
  emitEventOnEntity = (where,theEvent) => {
    where.emit(theEvent);
  }

  //verbanddoos openen
  openBox = () =>{
    if(step == 3){
      let att = document.createAttribute("animation");
      att.value = "property: rotation; easing: linear; dur: 1000; to: 0 180 -90 "
      verbanddoosDeur.setAttribute("animation", att.value);
      let att2 = document.createAttribute("animation__2");
      att.value = "property: position; easing: linear; dur: 1000; to: 3.5 1.05 -2.5 "
      verbanddoosDeur.setAttribute("animation__2", att.value);
    }else{console.log('openbox else');}
  }



  //-----------------
  //EINDE VERBAND INTERACTIE
  //-----------------


  //-----------------
  //TELEFOON INTERACTIE
  //-----------------

  let clicked = 0;
  phone.addEventListener('click', () => {
    // responsiveVoice.speak("hello world");
    if(clicked == 0){
      let att = document.createAttribute("animation");
      att.value = "property: position; easing: linear; dur: 1000; to: 1 2 -1 "
      phone.setAttribute("animation", att.value);
      clicked = 1;
    }else if(clicked == 1){
      let att = document.createAttribute("animation");
      att.value = "property: position; easing: linear; dur: 1000; to: 3 1.8 -7 "
      phone.setAttribute("animation", att.value);
      clicked = 0;
    }
    })

    let clicked2 = 0;
    phone2.addEventListener('click', () => {
      // responsiveVoice.speak("hello world");
      if(clicked2 == 0){
        let att = document.createAttribute("animation");
        att.value = "property: position; easing: linear; dur: 1000; to: 1 2 -1 "
        phone2.setAttribute("animation", att.value);
        clicked2 = 1;
      }else if(clicked2 == 1){
        let att = document.createAttribute("animation");
        att.value = "property: position; easing: linear; dur: 1000; to: 3 1.8 -7 "
        phone2.setAttribute("animation", att.value);
        clicked2 = 0;
      }
      })

    //-----------------
    //EINDE TELEFOON INTERACTIE
    //-----------------

    //------------------
    // Einde lvl 1
    //-----------------







//------------------------------------------------------------------------------------------------------------------------
    //-----------------
    //START VAN LEVEL 2
    //-----------------

    level2.addEventListener('click', () => {
      console.log("level2");
      if(level1Voldaan == true){
        //let att = document.createAttribute("animation");
        //att.value = "property: position; easing: linear; dur: 500; to: 0 -5 0 "
        sceneLevel2.setAttribute("animation", "property: position; easing: linear; dur: 10; to: 0 0 0" );
        setTimeout(function() {
          chalkboard.setAttribute("animation", "property: position; easing: linear; dur: 10; to: 0 10 0");
          chalkboard.setAttribute("visible","false");
          sceneLevel2.setAttribute("visible","true");
        },20);
        tooltipContainer.setAttribute("visible", true);

        if(step == 6){
          tooltipText.setAttribute("value", "Je hebt tijdens het koken in \nje hand gesneden, wat ga je \ndoen om de wond te verzorgen?");
          responsiveVoice.speak("Je hebt tijdens het koken in je arm gesneden, wat ga je doen om de wond te verzorgen?","Dutch Male");
        }
        arm.setAttribute('visible','false');
        armBebloed.setAttribute('visible','true');

      }else{
        console.log("Level 1 nog niet voldaan");

      }

    })

    //-----------------
    //CLEANINGINTERACTIE
    //-----------------
    let movedToTap2 = false
    //checks if it's time to clean your cut
    cleaningHandler = (e) => {
    console.log(step);
     if(step == 6 && movedToTap2 == false) {
       tooltipContainer.setAttribute("visible", false);
       //emit event to move to tap
       emitEventOnEntity(cameraEntity,'moveToTap');
       movedToTap2 = true;
       cleaningTimerCountdown('cleaning');
       cleaningText.setAttribute('visible','true');
       kraanAnimatie.setAttribute("visible", true);
       responsiveVoice.speak("Maak je wond schoon door het onder de kraan te houden.","Dutch Male");
     }else{console.log('Not at step 2 yet, or already at tap');}
    }

    cleaningInteractionPane.addEventListener('click', () => {
      cleaningHandler();
    })//cleaningInteractionPane
    //-----------------
    //EINDE CLEANINGINTERACTIE
    //-----------------

    // the timer functionality that is paused and played with the function coolingTimerCountdown
    let currentlyCleaning = false;
    let cleaningSeconds = 10;
    let progressBarWidth = 2.0;
    let progress = 2.0
    let cleaningTimer = setInterval(() => {
      if(currentlyCleaning == true){
        if(cleaningSeconds <= 10 && progressBarWidth > 0.40){
          cleaningSeconds -= 1;
          progress -= 0.20;
          progressBarWidth = progress.toFixed(2);
        } else if(cleaningSeconds <= 2){
          clearInterval(cleaningTimer);
          cleaningSeconds = 0;
          progressBarWidth = 0;
          cleaningProgressBar.setAttribute('visible','false');
          step += 1;
          console.log(step);
          emitEventOnEntity(cameraEntity,'goBack');
          cleaningText.setAttribute('visible','false');
          kraanAnimatie.setAttribute('visible', false);
        }
        cleaningProgressBar.setAttribute('width',progressBarWidth);
      }else{'not currently cooling'}
    },1000); //setInterval

    // the function compatible with the timer setinterval
    cleaningTimerCountdown = (status) => {
      switch(status){
        case 'cleaning':
          currentlyCleaning = true;
          break;
        case 'notCleaning':
          currentlyCleaning = false;
          clearInterval(cleaningTimer);
          break;
      }
    }// clleaningTimerCountdown

    let bandagedArm2 = false;
    let clickedVerband2 = false;
    bandageButton2.addEventListener('click', () => {
      if(step == 8 && bandagedArm2 == false && clickedVerband2 == true) {
        verband2.setAttribute('visible','false');
        bandageStart2.setAttribute('visible','false');
        cameraBandage2.setAttribute('visible','true');
        for(let i = 0; i != armEntity.length; i++){
          emitEventOnEntity(armEntity[i],'startBandaging');
        }
        emitEventOnEntity(cameraBandage2, 'startBandaging');
        armBebloed.addEventListener('animationcomplete__move3',() =>{
          certificate.setAttribute('visible','true');
          restart.setAttribute('visible', 'true');
          level2Voldaan = true;
        })
      }else{console.log('bandageButton else');}
    })//bandagebutton
    verbanddoosDeur2.addEventListener('click', () => {
      console.log("verbanddoosDeur2");
      openBox2();
    })
    verband2.addEventListener('click', () =>{
      if(step == 8 && clickedVerband2 == false){
        let att = document.createAttribute("animation");
        att.value = "property: position; easing: linear; dur: 1000; to: 1.3 1.4 -3 "
        verband2.setAttribute("animation", att.value);
        bandageStart2.setAttribute('visible','true');
        clickedVerband2 = true;
      }
    });
    //desinfectiemiddel oppakken
    let clickedMiddel = 0;
    middel.addEventListener('click', () => {
      // responsiveVoice.speak("hello world");
      if(clickedMiddel == 0 && step == 7){
        let att = document.createAttribute("animation");
        let att2 = document.createAttribute("animation");
        att.value = "property: position; easing: linear; dur: 1000; to: -0.5 1.4 -3 "
        att2.value = "property: rotation; easing: linear; dur: 1000; to: 0 80 0; startEvents: animationcomplete "
        middel.setAttribute("animation", att.value);
        middel.setAttribute("animation__2", att2.value);
        ontsmetStart.setAttribute('visible', 'true');
        ontsmetClicked = true;
        clickedMiddel = 1;
      }else{console.log("not at step 7 yet");}
      })
      //ontsmet interactie
      let ontsmetClicked = false;
      ontsmetButton.addEventListener('click', () => {
        if(step == 7 && ontsmetClicked == true){
          ontsmetAnimation.setAttribute('visible', 'true');
          for(let i = 0; i != armEntity.length; i++){
            emitEventOnEntity(armEntity[i], 'startOntsmetten')
          }
          emitEventOnEntity(ontsmetAnimation, 'startOntsmetten');
          middel.remove();
          let removeOntsmetButton = document.createAttribute("animation");
          removeOntsmetButton.value = "property: position; dur: 100; to: 0 80 0"
          ontsmetStart.setAttribute('visible','false');
          ontsmetStart.setAttribute('animation', removeOntsmetButton.value);
          step ++;
        }else{
          console.log('Ontsmet functie werkt niet');
        }
      });
    cameraBandage2.addEventListener('animationcomplete__5', () => {
      cameraBandage2.setAttribute('visible','false');
      armBebloed.setAttribute('visible','false');
      armVerband.setAttribute('visible','true');
      // emitEventOnEntity(armEntity, 'stopBandaging');
      for(let i = 0; i != armEntity.length; i++){
        emitEventOnEntity(armEntity[i],'stopBandaging');
      }
      step ++;
      bandagedArm2 = true;
      tooltipTextHandler();
    })
    openBox2 = () =>{
      if(step == 8) {
        let att = document.createAttribute("animation");
        att.value = "property: rotation; easing: linear; dur: 1000; to: 0 180 -90 "
        verbanddoosDeur2.setAttribute("animation", att.value);
        let att2 = document.createAttribute("animation__2");
        att.value = "property: position; easing: linear; dur: 1000; to: 3.5 1.05 -2.5 "
        verbanddoosDeur2.setAttribute("animation__2", att.value);
      }else{console.log('openbox2 else');}
    }
    //-----------------
    //EINDE VAN LEVEL 2
    //-----------------
//------------------------------------------------------------------------------------------------------------------------
//---------------------
//  Hover & feedback
//---------------------
  let basicButtonColorStandard = 'white';
  let basicButtonColorHover = 'orange';
  // hover feedback for buttons which have the class basicButton
  for(let i = 0; i != basicButtons.length; i++) {
    basicButtons[i].addEventListener('mouseenter', () => {
      basicButtons[i].setAttribute('color', basicButtonColorHover);
    })// E L
    basicButtons[i].addEventListener('mouseleave', () => {
      basicButtons[i].setAttribute('color', basicButtonColorStandard);
    })// E L
  } //forloop

  //gebied om de tekst heen
  let levelColorStandard = 'black';
  let levelColorHover = 'white';
  //tekst zelf
  let textColorHover = "black"
  let textColorStandard = "white"

    level1.addEventListener('mouseenter', () => {
      if(level1Voldaan == false){
      level1.setAttribute('color', levelColorHover);
      textLevel1.setAttribute('color', textColorHover)
    }else{
      console.log("nein");
    }
    })
    level1.addEventListener('mouseleave', () => {
      if(level1Voldaan == false){
      level1.setAttribute('color', levelColorStandard);
      textLevel1.setAttribute('color', textColorStandard);
    }else{
      level1.setAttribute('color', levelColorStandard);
      textLevel1.setAttribute('color', 'grey');
    }
    })
   level2.addEventListener('mouseenter', () => {
     if(level1Voldaan == true){
     level2.setAttribute('color', levelColorHover);
     textLevel2.setAttribute('color', textColorHover)
   }else{
     console.log("nope");
   }
   })
   level2.addEventListener('mouseleave', () => {
     if(level1Voldaan == true){
     level2.setAttribute('color', levelColorStandard);
     textLevel2.setAttribute('color', 'white');
   }else{
     level2.setAttribute('color', levelColorStandard);
     textLevel2.setAttribute('color', 'grey');
   }
   })

  }//window.onload
